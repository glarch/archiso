#!/bin/bash

set -e -u

repo_root=$(git rev-parse --show-toplevel)

iso_name='glarch'
iso_label="Glarch_$(date +%Y%m)"
iso_publisher='GLaTAN'
iso_application='Glarch Live/Rescue CD'
sfs_comp='zstd'

init_archiso() {
  rsync -r --delete /usr/share/archiso/configs/releng/ ${repo_root}/archlive/
}

overlay_config() {
  cp -rf ${repo_root}/glarch/archlive/ ${repo_root}/
}

replace_config() {
  sed -ie "s/mkarchiso/mkarchiso -c ${sfs_comp}/g" ${repo_root}/archlive/build.sh
}

build() {
  cd ${repo_root}/archlive/
  ./build.sh -v \
    -N ${iso_name} \
    -L ${iso_label} \
    -P ${iso_publisher} \
    -A ${iso_application}
}

main() {
  if [[ ${UID} -ne "0" ]]; then
    echo "Please run as root user."
  else
    init_archiso
    overlay_config
    replace_config
    build
  fi
}

main
