#!/bin/bash

# get config files
wget https://gitlab.com/glarch/config/-/archive/master/config-master.tar.bz2
tar -xf config-master.tar.bz2

# copy config
cp config-master/root/etc/makepkg.conf ../archive/airootfs/etc/makepkg.conf
cp config-master/root/etc/pacman.conf ../archive/pacman.conf
